index.html: index.adoc $(wildcard sections/*) $(wildcard assets/*) jandot.css
	bundle exec asciidoctor-revealjs index.adoc

clean:
	rm -f *.html
